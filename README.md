# Git Snippets

With Git Snippets you can share code snippets with the world from your website.

## Documentation

* [Wiki](https://gitlab.com/aochoae/git-snippets/wikis/home)
* [Snippets](https://docs.gitlab.com/ee/user/snippets.html)
* [Snippets API](https://docs.gitlab.com/ee/api/snippets.html)
* [Project snippets API](https://docs.gitlab.com/ee/api/project_snippets.html)

## Requirements

* PHP version 7.1 or greater
* WordPress 4.9 or greater

## Credits

* Icon made by [Freepik](https://www.flaticon.com/authors/freepik)
  from [www.flaticon.com](https://www.flaticon.com/free-icon/digital-interface-sign-of-motherboard_59367)

## License

This project is licensed under the GNU General Public License, Version 2.0.
See [LICENSE](LICENSE) for the full license text.

=== Git Snippets ===
Contributors: aochoae
Tags: git, gitlab, shortcode
Requires at least: 4.9
Tested up to: 5.8
Requires PHP: 7.1
Stable tag: 2.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

With Git Snippets you can share code and text fragments with other users from your website.

== Description ==

With Git Snippets you can share code and text fragments with other users from your website.

To embed a project snippet, first make sure that:

* The snippet is public
* In **Project > Settings > Visibility, project features, permissions**, the snippets permissions are set to **Everyone with access**

Use `[gitlab]` shortcode by using the ID or URL of the snippet in your posts and pages.

* `[gitlab id="2180458"]`
* `[gitlab url="https://gitlab.com/aochoae/git-snippets/-/snippets/2180458"]`

== Installation ==

= Minimum Requirements =

* WordPress 4.9 or greater
* PHP 7.1 or greater

= Manual installation =

1. Navigate to **Plugins > Add New**.
1. Click the Upload Plugin button at the top of the screen.
1. Select the zip file from your local filesystem.
1. Click the **Install Now** button.
1. Click the Activate Plugin button at the bottom of the page.

== Changelog ==

= 2.0.0 (September 25, 2021) =

* GitLab 14.1 (Tested)
* Code cleanup

= 1.0.0 (August 28, 2019) =

* Initial release.

<?php
/**
 * @link https://developer.wordpress.org/plugins/the-basics/uninstall-methods/
 *
 * @package GitSnippets
 */

/* if uninstall.php is not called by WordPress, exit */
defined( 'WP_UNINSTALL_PLUGIN' ) || exit;

/* Remove the 'gitlab_access_token' option. */
delete_option( 'gitlab_access_token' );

/* Removes all cache items. */
wp_cache_flush();

<?php
/**
 * Plugin Name: Git Snippets
 * Plugin URI: https://gitlab.com/aochoae/git-snippets
 * Description: With Git Snippets you can share code snippets with the world from your website.
 * Author: Luis A. Ochoa
 * Author URI: https://gitlab.com/aochoae/
 * Version: 2.0.0
 * License: GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain: git-snippets
 * Domain Path: /languages
 *
 * @package   GitSnippets
 * @author    Luis A. Ochoa
 * @copyright 2019-2021 Luis A. Ochoa
 * @license   GPL-2.0-or-later
 */

defined( 'ABSPATH' ) || exit;

if ( ! defined( 'GIT_SNIPPETS_FILE' ) ) {
    define( 'GIT_SNIPPETS_FILE', __FILE__ );
}

/* PHP namespace autoloader */
require_once( dirname( GIT_SNIPPETS_FILE ) . '/vendor/autoload.php' );

\GitSnippets\Loader::newInstance( plugin_basename( GIT_SNIPPETS_FILE ) );

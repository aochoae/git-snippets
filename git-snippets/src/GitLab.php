<?php
/**
 * @package GitSnippets
 */

namespace GitSnippets;

/**
 * GitLab class
 *
 * @since 1.0.0
 */
class GitLab
{
    /**
     * Constructor
     *
     * @since 1.0.0
     */
    public function __construct( $snippet_id )
    {
        $this->snippet_id = $snippet_id;
    }

    /**
     * Retrieves the URL of the snippet using the snippet's ID.
     *
     * @since 1.0.0
     */
    public function getSnippet()
    {
        $key = hash( 'md5', serialize( [ "gitlab-api-{$this->snippet_id}", GIT_SNIPPETS_FILE ] ) );

        if ( false === ( $response = wp_cache_get( $key, 'git_snippets' ) ) ) {

            $response = $this->getResponse();

            wp_cache_add( $key, $response );
        }

        return $response;
    }

    /**
     * @since 1.0.0
     */
    public function getResponse()
    {
        $snippet_id = $this->snippet_id;

        $url = "https://gitlab.com/api/v4/snippets/{$snippet_id}";

        $response = wp_remote_get( esc_url_raw( $url ) );

        $body = json_decode( wp_remote_retrieve_body( $response ), true );

        if ( 200 == wp_remote_retrieve_response_code( $response ) ) {
            return $body['web_url'] ?: '';
        }

        return new \WP_Error( "error", esc_html( $body['message'] ) );
    }
}

<?php
/**
 * @package GitSnippets
 */

namespace GitSnippets;

/**
 * GitLab class
 *
 * @since 1.0.0
 */
class Shortcode
{
    /**
     * Singleton instance
     *
     * @since 1.0.0
     * @var Shortcode
     */
    private static $instance;

    /**
     * Constructor.
     *
     * @since 1.0.0
     */
    private function __construct()
    {
        add_shortcode( 'gitlab', [ $this, 'gitlab' ] );
    }

    /**
     * The singleton method.
     *
     * @since 1.0.0
     *
     * @return Shortcode
     */
    public static function newInstance(): Shortcode
    {
        if ( ! isset( self::$instance ) ) {
            self::$instance = new Shortcode;
        }

        return self::$instance;
    }

    /**
     * GitLab shortcode.
     *
     * @since 1.0.0
     */
    public function gitlab( $atts )
    {
        $args = shortcode_atts( [
            'id'  => '',
            'url' => ''
        ], $atts );

        /* Snippet URL (User data) */
        $url = filter_var( $args['url'], FILTER_SANITIZE_URL );

        if ( ! empty( $url ) ) {
            return $this->getEmbedByUrl( $url );
        }

        /* Snippet ID (User data) */
        $id = filter_var( $args['id'], FILTER_SANITIZE_NUMBER_INT );

        if ( empty( $id ) ) {
            return '';
        }

        return $this->getEmbedById( $id );
    }

    /**
     * @since 2.0.0
     *
     * @param int $id
     * @return string
     */
    private function getEmbedById( int $id ) {

        /* GitLab response */
        $url = (new GitLab( $id ))->getSnippet();

        if ( is_wp_error( $url ) ) {
            return esc_html( $url->get_error_message() );
        }

        $url = filter_var( $url, FILTER_SANITIZE_URL );

        return !empty( $url ) ? $this->getEmbed( $url ) : "";
    }

    /**
     * @since 2.0.0
     *
     * @param string $url
     * @return string
     */
    private function getEmbedByUrl( string $url ) {

        $response = wp_remote_get( $url );

        $cookie = wp_remote_retrieve_cookie( $response, '_gitlab_session' );

        return is_a( $cookie, 'WP_Http_Cookie' ) ? $this->getEmbed( $url ) : "";
    }

    /**
     * Embed snippets.
     *
     * @since 2.0.0
     *
     * @param String $url
     * @return string
     */
    private function getEmbed( String $url ) {

        $url = esc_url_raw( "{$url}.js" ); // Snippet

        return "<script src=\"{$url}\"></script>";
    }
}
